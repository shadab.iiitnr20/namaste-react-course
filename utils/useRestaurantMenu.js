import { useState, useEffect } from "react";
import { FETCH_MENU_URL } from "./constants";

const useRestaurantMenu = (resId) => {
  const [restaurant, setRestaurant] = useState(null);

  useEffect(() => {
    getRestaurantDetails();
  }, []);

  async function getRestaurantDetails() {
    const data = await fetch(FETCH_MENU_URL + resId);
    const res = await data.json();
    setRestaurant(res);
  }

  return restaurant;
};

export default useRestaurantMenu;
