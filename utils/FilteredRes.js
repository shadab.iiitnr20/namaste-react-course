export function filteredRes(searchInput, listOfRestaurants) {
    const data = listOfRestaurants.filter((res) => {
      return res?.data?.name
        ?.toLowerCase()
        ?.includes(searchInput.toLowerCase());
    });
    return data;
  }