#Completing the Namaste React course by Akshay Saini

#Parcel

- Dev Build
- Local Server
- HMR Hot Module Replacement
- File Watching Algorithm - written in C++
- Caching - Faster Builds
- Image Optimization
- Minification
- Bundling
- Compressing
- Consistent Hashing
- Code Splitting
- Differential Bundling - Support Older Browser
- Diagnostic
- Error Hondling
- HTTPs
- Tree Shaking - remove unused code
- Different build for Dev and Prod
