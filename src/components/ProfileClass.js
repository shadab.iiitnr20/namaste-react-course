import React from "react";

class ProfileClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
    console.log("Constructor");
  }

  componentDidMount() {
    console.log("Component Did Mount");
  }

  render() {
    const { count } = this.state;
    console.log("Render");
    return (
      <div>
        <h2>{count}</h2>
        <h1>Hello World Class</h1>
        <button onClick={() => this.setState({ count: count + 1 })}>
          Increment
        </button>
      </div>
    );
  }
}

export default ProfileClass;

// import React from "react";

// class ProfileClass extends React.Component {
//   constructor(props) {
//     super(props);
//     //Create State Here
//     this.state = {
//       count: 0,
//       count2: 0,
//     };
//   }
//   render() {
//     const { count } = this.state;
//     return (
//       <div>
//         <h2>Profile Class Based Component</h2>
//         <h2>Name: {this.props.name}</h2>
//         <h2>{count}</h2>
//         {/* WE DO NOT MUTATE STATE DIRECTLY */}
//         {/* NEVER DO this.state = something */}
//         <button onClick={() => this.setState({ count: count + 1 })}>
//           Count
//         </button>
//       </div>
//     );
//   }
// }

// export default ProfileClass;
