const ShimmerResCard = () => {
  return (
    <>
      <div className="res-container">
        {Array(1)
          .fill("")
          .map((ele, index) => (
            <div key={index} className="shimmer-card-res"></div>
          ))}
      </div>
    </>
  );
};

export default ShimmerResCard;
