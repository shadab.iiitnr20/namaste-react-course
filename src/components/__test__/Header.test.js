import { render } from "@testing-library/react";
import Header from "../Header";
import { Provider } from "react-redux";
import store from "../../../utils/store";
import { StaticRouter } from "react-router-dom/server";
import { LOGO_URL } from "../../../utils/constants";

// test("Logo should load on the header", () => {
//   const header = render(
//     <StaticRouter>
//       <Provider store={store}>
//         <Header />;
//       </Provider>
//     </StaticRouter>
//   );
//   const logo = header.getByTestId("logo")
//   expect(LOGO_URL).toBe()

// });

test("Username should be present on the header from context API", () => {
    const header = render(
      <StaticRouter>
        <Provider store={store}>
          <Header />;
        </Provider>
      </StaticRouter>
    );
    const username = header.getByTestId("username")
    expect(username.innerHTML).toBe("Dummy Name")
  
  });


  test("Cart Length on Header check", () => {
    const header = render(
      <StaticRouter>
        <Provider store={store}>
          <Header />;
        </Provider>
      </StaticRouter>
    );
    const cart = header.getByTestId("cart")
    expect(cart.innerHTML).toBe("Cart - 0 items")
  
  });