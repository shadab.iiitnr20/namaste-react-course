import { useState, useContext } from "react";
import { LOGO_URL } from "../../utils/constants";
import { Link } from "react-router-dom";
import UserContext from "../../utils/UserContext";
import { useSelector } from "react-redux";

const Header = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const { user } = useContext(UserContext);

  const cartItems = useSelector((store) => store.cart.items);
  console.log(cartItems);

  return (
    <div className="flex justify-between bg-green-100 shadow-lg">
      <div className="logo-container">
        <img data-testid="logo" alt="Logo" className="h-28 p-2" src={LOGO_URL} />
      </div>
      <div className="nav-items">
        <ul className="flex py-10">
          <li className="px-2 font-semibold">
            <Link to="/">Home</Link>
          </li>
          <li className="px-2 font-semibold">
            <Link to="/about">About</Link>
          </li>
          <li className="px-2 font-semibold">
            <Link to="/contact">Contact Us</Link>
          </li>
          <li className="px-2 font-semibold">
            <Link to="/instamart">Instamart</Link>
          </li>
          <li className="px-2 font-semibold" data-testid="cart">
            Cart - {cartItems.length} items
          </li>
        </ul>
      </div>
      <span data-testid="username" className="m-10 text-red-800 font-bold">{user.name}</span>
      {isLoggedIn ? (
        <button
          className="rounded-xl m-2 p-2 bg-green-300 text-black font-bold shadow"
          onClick={() => setIsLoggedIn(false)}
        >
          Logout
        </button>
      ) : (
        <button
          className="rounded-xl m-2 p-2 bg-green-300 text-black font-bold shadow"
          onClick={() => setIsLoggedIn(true)}
        >
          Login
        </button>
      )}
    </div>
  );
};

export default Header;
