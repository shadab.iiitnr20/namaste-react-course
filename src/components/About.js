import React from "react";
import { Outlet } from "react-router-dom";
import ProfileClass from "../components/ProfileClass";
import Profile from "../components/Profile";

const About = () => {
  return (
    <>
      <div>
        <h1>About Us Page</h1>
        <p>This is About Us Page</p>
        <Profile name={"Shadab"} />
        <ProfileClass name={"ShadabClass"} />
      </div>
    </>
  );
};

export default About;
