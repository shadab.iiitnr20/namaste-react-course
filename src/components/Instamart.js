import { useState } from "react";

const Section = ({ title, description, isVisible, setIsVisible }) => {
  //const [isVisible, setIsVisible] = useState(false);
  return (
    <>
      <div className="border border-black text-lg p-2 m-2">
        <h3 className="font-semibold text-xl">{title}</h3>
        {isVisible ? (
          <>
            <button
              className="cursor-pointer underline"
              onClick={() => setIsVisible("")}
            >
              Hide
            </button>
            <p>{description}</p>
          </>
        ) : (
          <button
            className="cursor-pointer underline"
            onClick={() => setIsVisible()}
          >
            Show
          </button>
        )}
      </div>
    </>
  );
};

const Instamart = () => {
  const [visibleSection, setVisibleSection] = useState("about");
  return (
    <div>
      <h1 className="text text-3xl font-bold m-2 p-2">Instamart</h1>
      <Section
        title={"About Instamart"}
        description={"This is about section of instamart"}
        isVisible={visibleSection === "about"}
        setIsVisible={() => setVisibleSection("about")}
      />
      <Section
        title={"Team Instamart"}
        description={"This is team section of instamart"}
        isVisible={visibleSection === "team"}
        setIsVisible={() => setVisibleSection("team")}
      />
      <Section
        title={"Careers Instamart"}
        description={"This is careers section of instamart"}
        isVisible={visibleSection === "careers"}
        setIsVisible={() => setVisibleSection("careers")}
      />
    </div>
  );
};

export default Instamart;
