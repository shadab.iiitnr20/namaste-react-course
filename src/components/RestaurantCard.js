import { CDN_URL } from "../../utils/constants";
import { useContext } from "react";
import UserContext from "../../utils/UserContext";

const RestaurantCard = (props) => {
  const { resData } = props;
  const { user } = useContext(UserContext);

  const { cloudinaryImageId, name, cuisines, costForTwo, avgRating } =
    resData?.data;

  return (
    <div className="w-52 p-2 m-2 shadow-lg bg-lime-50">
      <img
        className="bg-white"
        alt="res-logo"
        src={CDN_URL + cloudinaryImageId}
      />
      <h3 className="font-bold">{name}</h3>
      <h4 className="font-small">{cuisines.join(", ")}</h4>
      <h4 className="font-small">₹{costForTwo / 100} FOR TWO</h4>
      <h4 className="font-small">{avgRating}</h4>
      <h5>{user.name}</h5>
    </div>
  );
};

export default RestaurantCard;
