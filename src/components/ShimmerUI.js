const Shimmer = () => {
  return (
    <>
      <div className="res-container" data-testid="shimmer">
        {Array(15)
          .fill("")
          .map((ele, index) => (
            <div key={index} className="shimmer-card"></div>
          ))}
      </div>
    </>
  );
};

export default Shimmer;
