import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ShimmerResCard from "../components/ShimmerResCard";
import { CDN_URL } from "../../utils/constants";
import useRestaurantMenu from "../../utils/useRestaurantMenu";
import { useDispatch } from "react-redux";
import { addItem } from "../../utils/cartSlice";

const RestaurantMenu = () => {
  const params = useParams();
  const { id } = params;

  const restaurant = useRestaurantMenu(id);
  //console.log(restaurant);
  const restaurantDetails = restaurant?.data?.cards[0]?.card?.card?.info;

  const dispatch = useDispatch();

  // const handleAddItem = () => {
  //   dispatch(addItem("Grapes"));
  // };

  const addFoodItem = (item) => {
    dispatch(addItem(item))
  }

  return !restaurant ? (
    <ShimmerResCard />
  ) : (
    <div className="flex">
      <div>
        {/* <h1>Restaurant ID: {id}</h1> */}
        <h2 className="m-2 p-2 text-4xl font-bold">
          {restaurantDetails?.name}
        </h2>
        <img
          className="m-2 p-2 w-56"
          src={CDN_URL + restaurantDetails?.cloudinaryImageId}
        />
        <h3 className="m-2 p-2 text-1xl font-semibold">
          {restaurantDetails?.locality}
        </h3>
        <h3 className="m-2 p-2 text-1xl font-semibold">
          {restaurantDetails?.city}
        </h3>
        <h3 className="m-2 p-2 text-1xl font-semibold">
          {restaurantDetails?.costForTwoMessage}
        </h3>
        <h3 className="m-2 p-2 text-1xl font-semibold">
          {restaurantDetails?.avgRating}
        </h3>
      </div>
      {/* <div>
        <button className="m-2 p-2 bg-green-200" onClick={() => handleAddItem()}>
          Add Item
        </button>
      </div> */}
      <div>
        <h1 className="m-2 p-2 text-2xl font-semibold underline">
          {
            restaurant?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR
              ?.cards[2]?.card?.card?.title
          }
        </h1>
        <ul>
          {(restaurant?.data?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[2]?.card?.card?.itemCards).map(
            (item) => {
              return (
                <li className="m-2 p-2" key={item?.card?.info?.id}>
                  {<h4>{item?.card?.info?.name}</h4>}
                  <button
                    onClick={() => addFoodItem(item)}
                    className="p-2 m-2 bg-lime-200 rounded-full"
                  >
                    Add Item
                  </button>
                </li>
              );
            }
          )}
        </ul>
      </div>
    </div>
  );
};

export default RestaurantMenu;
