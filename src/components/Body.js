import RestaurantCard from "./RestaurantCard";
//import resList from "../../utils/mockData";
import { useEffect, useState } from "react";
import Shimmer from "./ShimmerUI";
import { Link } from "react-router-dom";
import { filteredRes } from "../../utils/FilteredRes";
import useOnline from "../../utils/useOnline";
//import useRestaurantList from "../../utils/useRestaurantList";

const Body = () => {
  const [searchInput, setSearchInput] = useState("");
  //const [listOfRestaurants, filteredRestaurant] = useRestaurantList()
  const [listOfRestaurants, setListOfRestaurants] = useState([]);
  const [filteredRestaurant, setFilteredRestaurant] = useState([]);

  useEffect(() => {
    getRestaurants();
  }, []);

  async function getRestaurants() {
    const data = await fetch(
      "https://www.swiggy.com/dapi/restaurants/list/v5?lat=17.385044&lng=78.486671&page_type=DESKTOP_WEB_LISTING"
    );
    const json = await data.json();
    //console.log(json);
    setListOfRestaurants(json?.data?.cards[2]?.data?.data?.cards);
    setFilteredRestaurant(json?.data?.cards[2]?.data?.data?.cards);
  }

  const isOnline = useOnline();

  if (!isOnline) {
    return <h1>Offline, please check your internet connection!!</h1>;
  }

  if (!listOfRestaurants) return null;

  return listOfRestaurants?.length === 0 ? (
    <Shimmer />
  ) : (
    <div className="body">
      <div className="search-container p-5 bg-lime-50 my-5">
        <input
          type="text"
          data-testid="input-box"
          className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight"
          placeholder="Search"
          value={searchInput}
          onChange={(e) => setSearchInput(e.target.value)}
        />
        <button
          className="rounded-xl m-2 p-2 bg-lime-200 text-black font-bold shadow"
          data-testid="search-btn"
          onClick={() => {
            const data = filteredRes(searchInput, listOfRestaurants);
            setFilteredRestaurant(data);
          }}
        >
          Search
        </button>
      </div>
      <div className="filter">
        <button
          className="rounded-xl m-2 p-2 bg-lime-200 text-black font-bold shadow"
          onClick={() => {
            const filteredList = listOfRestaurants.filter((res) => {
              return res.data.avgRating > 4;
            });
            // console.log(filteredList);
            //setListOfRestaurants(filteredList);
          }}
        >
          Top Rated Restaurant
        </button>
      </div>
      <div className="flex flex-wrap h-fit" data-testid="res-list">
        {filteredRestaurant.length === 0 ? (
          <h1>No Match Found..!!</h1>
        ) : (
          filteredRestaurant.map((restaurant) => (
            <Link
              to={"/restaurant/" + restaurant?.data?.id}
              key={restaurant?.data?.id}
            >
              <RestaurantCard resData={restaurant} />
            </Link>
          ))
        )}
      </div>
    </div>
  );
};

export default Body;
